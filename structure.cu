#include <stdio.h>
#include <malloc.h>

#define MIN(x,y) ((x)<(y)?(x):(y))
#define SQR(x) ((x)*(x))

#define MAX_THREADS 1024

typedef struct  {
	float max;
	int  distance_sqr;
} diff_record;

static void print_time() {

        time_t t;
        time(&t);
	
}

void testing(const char *file) {
	typedef struct { int distance; float change; } table;
	FILE *f;
	int i, j, k, ii, jj, index, dimension, num_distances, distance;
	float **map, diff;
	table *results;

	puts("Test");
	f = fopen(file,"r");
	fscanf(f,"%d\n",&dimension);
	map = (float**)malloc(dimension*sizeof(float*));
	for (i=0; i<dimension; ++i) map[i] = (float*)malloc(dimension*sizeof(float));
	for (i=0; i<dimension; ++i)
		for (j=0; j<dimension; ++j) fscanf(f,"%f\n",&map[i][j]);
	fclose(f);


	num_distances = (dimension*(dimension+1))/2-1;
	results = (table*)malloc(num_distances*sizeof(table));
	
   	index = 0; 
    	for (i=0; i<dimension; ++i) {
           for (j=(i==0?1:i); j<dimension; ++j) {
            results[index].distance = SQR(i)+SQR(j); results[index].change = -2;
            
            ++index;
           }
       
        } 


	// DO the cut
	for (i=0; i<dimension; ++i)
	   for (j=0; j<dimension; ++j) {
		for (ii=0; ii<dimension; ++ii)
	   	    for (jj=0; jj<dimension; ++jj) {
			distance = SQR(i-ii)+SQR(j-jj); 
			diff = fabs(map[ii][jj]-map[i][j]);
				for (k=0; k<num_distances; ++k)
					if ( results[k].distance == distance && diff > results[k].change ) results[k].change = diff;
		
		    }
           }

	for (i=0; i<num_distances; ++i) printf("%d %f\n",results[i].distance,results[i].change);
}
	

static void printDevProp(cudaDeviceProp devProp)
{ 
    printf("Major revision number:         %d\n",  devProp.major);
    printf("Minor revision number:         %d\n",  devProp.minor);
    printf("Name:                          %s\n",  devProp.name);
    printf("Total global memory:           %u\n",  devProp.totalGlobalMem);
    printf("Total shared memory per block: %u\n",  devProp.sharedMemPerBlock);
    printf("Total registers per block:     %d\n",  devProp.regsPerBlock);
    printf("Warp size:                     %d\n",  devProp.warpSize);
    printf("Maximum memory pitch:          %u\n",  devProp.memPitch);
    printf("Maximum threads per block:     %d\n",  devProp.maxThreadsPerBlock);
    for (int i = 0; i < 3; ++i)
    	printf("Maximum dimension %d of block:  %d\n", i, devProp.maxThreadsDim[i]);
    for (int i = 0; i < 3; ++i)
    	printf("Maximum dimension %d of grid:   %d\n", i, devProp.maxGridSize[i]);
    printf("Clock rate:                    %d\n",  devProp.clockRate);
    printf("Total constant memory:         %u\n",  devProp.totalConstMem);
    printf("Texture alignment:             %u\n",  devProp.textureAlignment);
    printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
    printf("Number of multiprocessors:     %d\n",  devProp.multiProcessorCount);
    printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
}

static cudaDeviceProp device_info(int do_print) {
    // Number of CUDA devices
    int devCount;
    cudaDeviceProp devProp;

    cudaGetDeviceCount(&devCount);
    if ( do_print ) { 
	printf("CUDA Device Query...\n");
    	printf("There are %d CUDA devices.\n", devCount);
    }

    // Iterate through devices
    for (int i = 0; i < devCount; ++i)
    {
        // Get device properties
        if ( do_print ) printf("\nCUDA Device #%d\n", i);
        cudaGetDeviceProperties(&devProp, i);
        if ( do_print ) printDevProp(devProp);
	else return devProp;
    }
 
     return devProp;
}

   
__global__ void process(int dimension, diff_record *results, float *map, int num_distances) {
    int i, index, interval, distance_between, distance_sqr, effective_block_id;
    int x1, y1, x2, y2;
    float diff;
    diff_record __shared__ thread_results[MAX_THREADS];
	
    // All threads in this block are doing the same distance.

    effective_block_id = blockIdx.y;	// pretend to be many blocks as there won't be enough

    while ( effective_block_id < num_distances ) {
    
    thread_results[threadIdx.y].max = -1e38;  

    // Work out which path distance I do, and get the index into the table of paths
    index = effective_block_id; 
    distance_sqr = results[index].distance_sqr; 
	//printf("block %d thread %d index %d distance %d\n",effective_block_id,threadIdx.y,index,distance_sqr);
    interval = threadIdx.y;		// All threads do all the intervals 1..dimension
   
    while ( interval < dimension*dimension ) {
	if ( interval > 0 ) {
				//printf("block %d doing distance %d thread %d doing interval %d\n",effective_block_id,distance_sqr,threadIdx.y,interval);
	    //Flatten map into 1-D and scan 
	    for (i=0; i<dimension*dimension-interval; ++i) {
	
        	     // This point
        	    x1 = i/dimension; y1 = i%dimension;  // This point
        	    x2 = (i+interval)/dimension; y2 = (i+interval)%dimension;  // Next point
		
        	    distance_between = SQR(x2-x1)+SQR(y2-y1);
				//printf("between (%d %d) (%d %d) %f %f %f %d\n",x1,y1,x2,y2,map[i+interval],map[i],fabs(map[i+interval]-map[i]),distance_sqr);
        	    if ( distance_between == distance_sqr ) {		// collect stats
			diff = fabs(map[i+interval]-map[i]);
			if ( diff > thread_results[threadIdx.y].max ) thread_results[threadIdx.y].max = diff;
			
		    }
            } 
        }
	interval += blockDim.y;
    }
	

    __syncthreads();

 
    // merge these interval results into the global array. some threads might not have found distance_sqr, so use 'count'
    if ( threadIdx.y == 0 ) {
	
	results[index].max = -1e38; 

	for (i=0; i<blockDim.y; ++i) 
	    if ( thread_results[i].max >= 0 ) {
		if ( thread_results[i].max > results[index].max ) results[index].max = thread_results[i].max;
	    }		
    }

    effective_block_id += gridDim.y;
    }
}

int main(int argc, char *argv[]) { 
    int num_distances, i, j, k, distance, dimension;
    FILE *f;
    float *device_map_memory, *map_memory;
    diff_record *device_output, *output;
    cudaDeviceProp device;
    dim3 grid_dim, block_dim;

    if ( strcmp("-test",argv[1]) == 0 ) { testing(argv[2]); exit(0); }
    puts("Real");
    
    if ( (f = fopen(argv[2],"r")) == NULL ) {
	fprintf(stderr,"Failed to open %s\n",argv[2]);
	exit(1);
    }
    fscanf(f,"%d\n",&dimension); 
    fclose(f);

    num_distances = (dimension*(dimension+1))/2-1;
    if ( (output=(diff_record*)malloc(num_distances*sizeof(diff_record))) == NULL ) {
	fprintf(stderr,"Failed to allocate host memory for output table.\n"); exit(1);
    } 
    for(i=0; i<num_distances; ++i) output[i].distance_sqr = -1;

    for (i=0; i<dimension; ++i) {
        for (j=(i==0?1:i); j<dimension; ++j) {
            distance = SQR(i)+SQR(j);
           
            for (k=0; output[k].distance_sqr != -1 && output[k].distance_sqr != distance && k != num_distances; ++k);
	    if ( k == num_distances ) {
		fprintf(stderr,"Error: ran out of distances\n");
		return 1;
	    }
	    output[k].distance_sqr = distance;
	
        }
       
    } 
    for (k=0; output[k].distance_sqr != -1 && k != num_distances; ++k);
    printf("Num distances %d reduced to %d\n",num_distances,k);
    num_distances = k; 
    if ( (output=(diff_record*)realloc(output,num_distances*sizeof(diff_record))) == NULL ) {
	fprintf(stderr,"Failed to realloc\n"); return 1;
    }

    if ( cudaMalloc(&device_output,num_distances*sizeof(diff_record))  != cudaSuccess ) {
	fprintf(stderr,"Failed to allocate GPGPU memory for output table.\n"); exit(1);
    } 
    if ( cudaMemcpy(device_output, output, num_distances*sizeof(diff_record),cudaMemcpyHostToDevice) != cudaSuccess ) {
	fprintf(stderr,"Failed to copy map to device\n"); exit(1);
    }     

    if ( cudaMalloc(&device_map_memory,dimension*dimension*sizeof(float))  != cudaSuccess ) {
	fprintf(stderr,"Failed to allocate GPGPU memory for map.\n"); exit(1);
    } 

	

    if ( (map_memory=(float*)malloc(dimension*dimension*sizeof(float))) == NULL ) {
	fprintf(stderr,"Failed to allocate host memory for map.\n"); exit(1);
    }    



 	// Load the map
    if ( (f = fopen(argv[2],"r")) == NULL ) {
	fprintf(stderr,"Failed to open %s\n",argv[2]);
	exit(1);
    }
    fscanf(f,"%d\n",&dimension); 
    for (i=0; i<dimension*dimension; ++i) {
	fscanf(f,"%f\n",map_memory+i);
    }
    fclose(f);

    if ( cudaMemcpy(device_map_memory, map_memory, dimension*dimension*sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess ) {
	fprintf(stderr,"Failed to copy map to device\n"); exit(1);
    }     

    device = device_info(0);

    grid_dim.x = 1; 						/* <- number of blocks in X direction. */
    grid_dim.y  = MIN(num_distances,device.maxGridSize[1]); 					/* <- number of blocks in Y direction. */
    block_dim.x = 1; 						/* <- number of threads in X direction. */
    block_dim.y = MIN(MIN(MAX_THREADS,device.maxThreadsDim[1]),dimension*dimension); 			/* <- number of threads in Y direction.*/
    
 
    printf("Dims %d %d\n",grid_dim.y,block_dim.y);
    print_time();
    fflush(stdout); return 1;
    process<<<grid_dim, block_dim>>>(dimension,device_output,device_map_memory,num_distances);
   
    print_time();
    if ( cudaMemcpy(output, device_output, num_distances*sizeof(diff_record), cudaMemcpyDeviceToHost) != cudaSuccess ) {
	fprintf(stderr,"Failed to copy result structure from host to device\n"); exit(1);
    }     

    for (i=0; i<num_distances; ++i)
            printf("%d %f\n",output[i].distance_sqr,output[i].max);
           

    cudaFree(device_map_memory);
    cudaFree(device_output);
 
    return 0;       
}
