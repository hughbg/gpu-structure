# Structure Function

The term structure function has a few different meanings and in this case is loosely defined as per the description in this paper: [The University of Michigan radio astronomy data base. I - Structure function analysis and the relation between BL Lacertae objects and quasi-stellar objects](http://articles.adsabs.harvard.edu//full/1992ApJ...396..469H/0000470.000.html)
It means to gather some kind of statistic that relates the value of each point in the image to all the other points.
This particular program finds the difference in value between pairs.
From this a histogram of differences can be obtained; showng, for example, that the difference in value is all over the place, or has 
certain common values. Suppose you had an image containing pixels that were either 0 or 1. Then the only differences between pixel values
in the image are 0 or 1.

The process of examining pairs of points is massively parallel, and lends itself to GPUS.
